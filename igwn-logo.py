#!/usr/bin/env python3
# Copyright (C) 2021 Cardiff University
# SPDX-License-Identifier: MIT

"""Generate an IGWN logo

This script generates a logo-style waveform image using PyCBC to generate
(by default) a 200/200 BBH signal.
"""

__author__ = "Duncan Macleod <duncan.macleod@ligo.org>"

import argparse
import sys

from matplotlib import (
    rcParams,
    pyplot,
)

from pycbc.waveform import get_td_waveform

rcParams.update({
    "figure.subplot.bottom": 0.,
    "figure.subplot.top": 1.,
    "figure.subplot.left": 0.,
    "figure.subplot.right": 1.0,
})


def create_parser():
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "-o",
        "--outfile",
        default="igwn-logo.png",
        help="filename to save logo into",
    )
    parser.add_argument(
        "-c",
        "--linecolor",
        "--linecolour",
        dest="linecolor",
        default="black",
        help="colour for logo",
    )
    return parser


def generate_waveform(mass=200, delta_t=1/16384., f_lower=20, **kwargs):
    hp, _ = get_td_waveform(
        approximant="IMRPhenomD",
        mass1=mass,
        mass2=mass,
        delta_t=delta_t,
        f_lower=f_lower,
        **kwargs,
    )
    start = -.2
    end = .05
    return hp.crop(start-hp.start_time, hp.end_time-end) * 1e18


def plot(data, outfile="igwn-logo.png", color="black"):
    # plot the waveform
    res = 1024
    dpi = 256
    inches = res / dpi
    fig = pyplot.figure(figsize=(inches, inches))
    ax = fig.gca()
    ax.plot(
        data.sample_times,
        data,
        color=color,
        linestyle="-",
        linewidth=8,
        solid_capstyle="round",
    )
    ax.set_axis_off()
    fig.tight_layout(pad=-0.5)
    fig.savefig(outfile, transparent=True)


def main(args=None):
    parser = create_parser()
    args = parser.parse_args(args=args)
    hoft = generate_waveform()
    plot(
        hoft,
        outfile=args.outfile,
        color=args.linecolor,
    )


if __name__ == "__main__":
    sys.exit(main())
